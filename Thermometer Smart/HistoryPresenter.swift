//
//  HistoryPresenter.swift
//  Thermometer Smart
//
//  Created by MinhNT-Mac on 6/6/19.
//  Copyright © 2019 MinhNT. All rights reserved.
//

import Foundation
protocol HistoryView: class {
    func refreshTableView()
}


protocol HistoryPeresenter {
    
    func viewDidLoad()
    var numberOfList:Int {get}
    func getWarningEntity(row:Int) -> WarningDetailEntity
}

class HistoryPresenterImplementation: HistoryPeresenter{
    fileprivate weak var view: HistoryView?
    var homeGateway: HomeGateway?
    var user: UserDetailEntity = UserDetailEntity.getUser()!
    
    var listWarning:[WarningDetailEntity] = []
    var numberOfList: Int {
        return listWarning.count
    }
    init(view: HistoryView, homeGateway: HomeGateway) {
        self.view = view
        self.homeGateway = homeGateway
    }
    
    func viewDidLoad() {
        getListHistory()
    }
   
    func getWarningEntity(row:Int) -> WarningDetailEntity {
        return listWarning[row]
    }
    
    func getListHistory() {
        homeGateway?.getListWarning(id: user._id, completetionHandler: { (result) in
            switch result {
            case let .success(data):
                if data.code == 200 {
//                    guard data.today.count != 0 else { return }
                    self.listWarning = data.year
                    DataSingleton.numberWarningOfDay = data.today.count
                    DataSingleton.numberWarningOfMonth = data.month.count
                    self.view?.refreshTableView()
                }else {
                }
                break
            case let .failure(error):
                break
            }
        })
    }
    
}
