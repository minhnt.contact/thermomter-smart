//
//  MainViewController.swift
//  Thermometer Smart
//
//  Created by MinhNT on 6/3/19.
//  Copyright © 2019 MinhNT. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {

    @IBOutlet weak var imgHome: UIImageView!
    @IBOutlet weak var cvTemperature: UIView!
    @IBOutlet weak var lbSettings: UILabel!
    @IBOutlet weak var lbHistory: UILabel!
    @IBOutlet weak var imgSettings: UIImageView!
    @IBOutlet weak var imgHistory: UIImageView!
    @IBOutlet weak var lbHome: UILabel!
    @IBOutlet weak var cvSettings: UIView!
    @IBOutlet weak var cvHistory: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        handlerShow(valute:MenuView.viewHome)
    }
    func handlerShow(valute:String) {
        switch valute {
        case MenuView.viewHome:
            cvTemperature.isHidden = false
            cvHistory.isHidden = true
            cvSettings.isHidden = true
            imgHome.image = UIImage(named: "icon_home")
            imgHistory.image = UIImage(named: "icon_history_black")
            imgSettings.image = UIImage(named: "icon_settings_black")
            lbHome.textColor = .white
            lbHistory.textColor = .black
            lbSettings.textColor = .black
            
        case MenuView.viewHistory:
            cvTemperature.isHidden = true
            cvHistory.isHidden = false
            cvSettings.isHidden = true
            imgHome.image = UIImage(named: "icon_home_black")
            imgHistory.image = UIImage(named: "icon_history")
            imgSettings.image = UIImage(named: "icon_settings_black")
            lbHome.textColor = .black
            lbHistory.textColor = .white
            lbSettings.textColor = .black
            
        case MenuView.viewSettings:
            cvTemperature.isHidden = true
            cvHistory.isHidden = true
            cvSettings.isHidden = false
            imgHome.image = UIImage(named: "icon_home_black")
            imgHistory.image = UIImage(named: "icon_history_black")
            imgSettings.image = UIImage(named: "icon_settings")
            lbHome.textColor = .black
            lbHistory.textColor = .black
            lbSettings.textColor = .white
        default:
            break
        }
        
    }
    
    @IBAction func showHome(_ sender: Any) {
        handlerShow(valute: MenuView.viewHome)
    }
    @IBAction func showHistory(_ sender: Any) {
        handlerShow(valute: MenuView.viewHistory)
    }
    
    @IBAction func showSettings(_ sender: Any) {
        handlerShow(valute: MenuView.viewSettings)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
}
