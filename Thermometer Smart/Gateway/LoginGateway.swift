//
//  LoginGateway.swift
//  Thermometer Smart
//
//  Created by MinhNT-Mac on 6/3/19.
//  Copyright © 2019 MinhNT. All rights reserved.
//

import Foundation

typealias LoginCompletionHandler = (_ user: Result<UserEntity>) -> Void


protocol LoginGateway {
    func login(username: String, password: String, completetionHandler: @escaping LoginCompletionHandler)
    func signup(username:String,fullName:String, password:String,completetionHandler: @escaping LoginCompletionHandler)
}
