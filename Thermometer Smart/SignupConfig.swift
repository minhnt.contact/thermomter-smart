//
//  SignupConfig.swift
//  Thermometer Smart
//
//  Created by MinhNT-Mac on 6/3/19.
//  Copyright © 2019 MinhNT. All rights reserved.
//
import Foundation
protocol SignupConfiguration {
    func configure(signupControler: SignupViewController)
}
class SignupConfigurationImplementation:  SignupConfiguration {
    func configure(signupControler: SignupViewController) {
        
        let router = SignupRouterImplemetation(signupController: signupControler)
        let loginGateway = ApiLoginGatewayImplementation()
        let presenter = SignupPresenterImplementation(view: signupControler, router: router, loginGateway: loginGateway)
        
        signupControler.presenter = presenter
    }
    
    
}
