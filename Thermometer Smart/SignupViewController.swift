//
//  SignupViewController.swift
//  Thermometer Smart
//
//  Created by MinhNT-Mac on 6/2/19.
//  Copyright © 2019 MinhNT. All rights reserved.
//

import UIKit

class SignupViewController: BaseViewController,SignupView {

    @IBOutlet weak var tfFullName: UITextField!
    @IBOutlet weak var tfUserName: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    @IBOutlet weak var tfRePassword: UITextField!
    @IBOutlet weak var btnSignup: UIButton!
    var presenter: SignupPresenter?
    var config: SignupConfiguration = SignupConfigurationImplementation()
    override func viewDidLoad() {
        super.viewDidLoad()
        config.configure(signupControler: self)
        presenter?.viewDidLoad()
        setupView()
    }
    func setupView(){
        btnSignup.setViewRadius()
    }
    
    @IBAction func btnSignup(_ sender: Any) {
        if tfFullName.text == "" || tfUserName.text == "" || tfPassword.text == "" || tfRePassword.text == "" {
            self.handleError(title: "Error", content:"Please enter full information to login")
            return
        }
        presenter?.handlerSignup(username: tfUserName.text!, fullName: tfFullName.text!, password: tfPassword.text!)
    }
    
    @IBAction func signin(_ sender: Any) {
        presenter?.presentSignin()
    }
    func handleError(title: String, content: String) {
        self.showAlertWithOnlyCancelAction(title: title, message: content, alertType: .alert, cancelTitle: "Ok", cancelActionHandler: nil)
    }
    
}
