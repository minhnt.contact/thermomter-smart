//
//  ApiLoginGateway.swift
//  Thermometer Smart
//
//  Created by MinhNT-Mac on 6/2/19.
//  Copyright © 2019 MinhNT. All rights reserved.
//

import Foundation
protocol ApiLoginGateway: LoginGateway {
    
}

class ApiLoginGatewayImplementation: ApiLoginGateway
{
    
    func login(username: String, password: String, completetionHandler: @escaping LoginCompletionHandler) {
        apiProvider.request(TSAPI.login(username,password))
            .asObservable().mapObject(UserEntity.self).subscribe(onNext:{(result) in
            completetionHandler(.success(result))
        }, onError:{(error) in
            completetionHandler(.failure(error))
        })
    }
    func signup(username: String, fullName: String, password: String, completetionHandler: @escaping LoginCompletionHandler) {
        apiProvider.request(TSAPI.signup(username, fullName, password))
            .asObservable().mapObject(UserEntity.self).subscribe(onNext:{(result) in
                completetionHandler(.success(result))
            }, onError:{(error) in
                completetionHandler(.failure(error))
            })
    }
    
    
}
