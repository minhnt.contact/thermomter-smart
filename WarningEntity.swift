//
//  WarningEntity.swift
//  Thermometer Smart
//
//  Created by MinhNT on 6/6/19.
//  Copyright © 2019 MinhNT. All rights reserved.
//

import Foundation

import RealmSwift
import ObjectMapper
class WarningEntity: Object, Mappable {
     var code:Int = 0
    var today:[WarningDetailEntity] = []
     var month:[WarningDetailEntity] = []
     var year:[WarningDetailEntity] = []
    
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        code <- map["code"]
        today <- map["today"]
        month <- map["month"]
        year <- map["year"]
        
    }
}
