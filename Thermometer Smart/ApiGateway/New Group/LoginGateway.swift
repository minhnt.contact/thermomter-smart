//
//  LoginGateway.swift
//  Thermometer Smart
//
//  Created by MinhNT-Mac on 6/2/19.
//  Copyright © 2019 MinhNT. All rights reserved.
//

import Foundation
typealias LoginCompletionHandler = (_ user: Result<UserEntity>) -> Void

protocol LoginGateway {
    func login(username:String , password:String, completetionHandler: @escaping LoginCompletionHandler)
}
