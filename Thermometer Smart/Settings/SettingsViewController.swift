//
//  SettingsViewController.swift
//  Thermometer Smart
//
//  Created by MinhNT on 6/4/19.
//  Copyright © 2019 MinhNT. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var swWarning: UISwitch!
    @IBOutlet weak var swModeAnalysis: UISwitch!
    @IBOutlet weak var tfPhoneSOS: UITextField!
    @IBOutlet weak var tfSafetyBot: UITextField!
    @IBOutlet weak var tfSafetyTop: UITextField!
    @IBOutlet weak var lbTimeRepeat: UILabel!
    @IBOutlet weak var imgAvatar: UIImageView!
    @IBOutlet weak var lbName: UILabel!
    @IBOutlet weak var lbNWD: UILabel!
    @IBOutlet weak var lbNWW: UILabel!
    @IBOutlet weak var pkOption: UIPickerView!
    @IBOutlet weak var lbRingTone: UILabel!
    var user:UserDetailEntity = UserDetailEntity.getUser()!
    var listRingTone:[String] = ["alarm_tone","warning","alarm_tone_2"]
    var listTimeWarning:[String] = ["1 minutes","5 minutes ","10 minutes"]
    var dataSource:[String] = []
    var isShowPicker:Bool = false
    var pkType:String = PickerView.RingTone
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupview()
        configSettings()
        handlerPKOption()
        dataSource = listTimeWarning
    }
    func setupview(){
        lbName.text = user.fullname
        imgAvatar.layer.cornerRadius = imgAvatar.frame.width / 2
        pkOption.delegate = self
        pkOption.dataSource = self
        tfSafetyBot.delegate = self
        tfSafetyTop.delegate = self
        tfPhoneSOS.delegate = self
        swModeAnalysis.addTarget(self, action: #selector(switchChanged), for: .valueChanged)
        lbNWD.text = String(DataSingleton.numberWarningOfDay)
        lbNWW.text = String(DataSingleton.numberWarningOfMonth)
    }
    
    
    func configSettings(){
        DispatchQueue.main.async {
            guard let configSettings = SettingsEntity.getSettings() else {
                return
            }
            self.swModeAnalysis.setOn(configSettings.isModeAnalysis, animated: true)
            self.tfPhoneSOS.text = String(configSettings.phoneSOS)
            self.tfSafetyBot.text = String(configSettings.temperatureBot)
            self.tfSafetyTop.text = String(configSettings.temperatureTop)
            self.lbTimeRepeat.text = "\(configSettings.timeRepeat) minutes"
            guard let user = UserDetailEntity.getUser() else {
                return
            }
            self.lbName.text = user.fullname
        }
        
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == tfSafetyBot {
            if tfSafetyBot.text == "" || tfSafetyBot.text?.doubleValue ?? 0 <= 30.0 {
                tfSafetyBot.text = "35"
            }
            DataSingleton.TemperatureBot = tfSafetyBot.text!.doubleValue
//            guard let configSettings = SettingsEntity.getSettings() else {
//                return
//            }
//            SettingsEntity.saveSettings(configSettings)
//
//            configSettings.temperatureBot = tfSafetyBot.text!.doubleValue
        }
        if textField == tfSafetyTop {
            if tfSafetyTop.text == "" || tfSafetyTop.text?.doubleValue ?? 0 >= 40.0 {
                tfSafetyTop.text = "37"
            }
            DataSingleton.TemperaureTop = tfSafetyBot.text!.doubleValue
//            guard let configSettings = SettingsEntity.getSettings() else {
//                return
//            }
//
//            configSettings.temperatureTop = tfSafetyTop.text!.doubleValue
//            SettingsEntity.saveSettings(configSettings)
        }
        if textField == tfPhoneSOS {
            guard let configSettings = SettingsEntity.getSettings() else { return }
            configSettings.phoneSOS = Int(tfPhoneSOS?.text ?? "115") ?? 115
            configSettings.phoneSOS = Int(tfPhoneSOS?.text ?? "115") ?? 115
            SettingsEntity.saveSettings(configSettings)
        }
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    
    @objc func switchChanged(swModeAnalysis: UISwitch) {
       DataSingleton.modeAnalysis = !DataSingleton.modeAnalysis
    }
    
    @IBAction func hiddenPk(_ sender: Any) {
        pkOption.isHidden = true
    }
    
    @IBAction func dissconnect(_ sender: Any) {
        let alert = UIAlertController(title: "Message", message: "Do you want dissconnect to device ?", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
            NotificationCenter.default.post(name: notificationName.disconnected.notification, object: nil)
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "searchingBluetooth") as! SearchingBluetoothViewController
            self.present(vc, animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
        
    }
    @IBAction func logout(_ sender: Any) {
        let alert = UIAlertController(title: "Message", message: "Do you want logout ?", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
            NotificationCenter.default.post(name: notificationName.disconnected.notification, object: nil)
            UserDetailEntity.deleteUser()
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "signin") as! LoginViewController
            self.present(vc, animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    @IBAction func showPkRepeat(_ sender: Any) {
         dataSource = listTimeWarning
        pkType = PickerView.TimeRepeat
        handlerPKOption()
        pkOption.reloadAllComponents()
       
        
    }
    func handlerPKOption() {
        isShowPicker = !isShowPicker
        if isShowPicker {
            pkOption.isHidden = true
        }else {
            pkOption.isHidden = false
        }
    }
    @IBAction func showPkRingTone(_ sender: Any) {
        dataSource = listRingTone
        pkType = PickerView.RingTone
        handlerPKOption()
        pkOption.reloadAllComponents()
    }
    
    func showPickerView(value: String){
        switch value {
        case PickerView.RingTone:
            break
        case PickerView.TimeRepeat:
            break
        default:
            break
        }
    }
    
    
}

extension SettingsViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return dataSource.count
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if self.pkType == PickerView.TimeRepeat {
            lbTimeRepeat.text = dataSource[row]
            switch row {
            case 0 :
                DataSingleton.TimeWarning = 1
                break
            case 1 :
                DataSingleton.TimeWarning = 5
                break
            case 2 :
                DataSingleton.TimeWarning = 10
                break
            default:
                break
            }
        }else {
            lbRingTone.text = dataSource[row]
            DataSingleton.RingTone = dataSource[row]
        }
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return dataSource[row]
    }
    
    
}


extension String {
    var doubleValue: Double {
        return Double(self) ?? 0
    }
}
