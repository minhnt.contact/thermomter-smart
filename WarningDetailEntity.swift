//
//  WarningDetailEntity.swift
//  Thermometer Smart
//
//  Created by MinhNT on 6/6/19.
//  Copyright © 2019 MinhNT. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper
class WarningDetailEntity: Object, Mappable {
    @objc dynamic var _id:Int = 0
    @objc dynamic var isWarning: Bool = true
    @objc dynamic var temperature: String = ""
    @objc dynamic var ownerId: String = ""
    @objc dynamic var createdAt: Double = 0.0
    @objc dynamic var year: Int = 0
    
    override static func primaryKey() -> String? {
        return "_id"
    }
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        _id <- map["_id"]
        isWarning <- map["isWarning"]
        temperature <- map["temperature"]
        ownerId <- map["ownerId"]
        createdAt <- map["createdAt"]
        year <- map["year"]
    }
}
