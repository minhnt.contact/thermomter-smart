//
//  TSAPI.swift
//  BaseSwift
//
//  Created by nava on 7/13/18.
//  Copyright © 2018 nava. All rights reserved.
//

import Foundation


import UIKit
import Moya
import RxSwift
extension TSAPI:TargetType
{
    var baseURL: URL {
        return URL(string: Config.rootUrl)!
    }
    
    var path: String {
        switch self {
        case .login:
            return "/_api/v1/account/login"
        case .signup:
            return "/_api/v1/account/register"
        case .warning:
            return "/_api/v1/warning"
        case .getHistory(let id):
            return "/_api/v1/warning/\(id!)"
        default:
            return ""
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .login,.signup, .warning:
            return .post
        default:
            return .get
        }
    }
    
    
    public var parameterEncoding: ParameterEncoding {
        switch self {
        case .getHistory:
            return URLEncoding.default
        default:
            return JSONEncoding.default
        }
    }
    
    
    var sampleData: Data {
        switch self {
        default:
            guard let url = Bundle.main.url(forResource: "Demo", withExtension: "json"),
                let data = try? Data(contentsOf: url) else {
                    return Data()
            }
            return data
        }
    }
    public var parameters: [String : Any]? {
        switch self {
        case .login(let username, let password):
            var paramester: [String: Any]?{
                var parameter:[String:Any] = [:]
                parameter["username"] = username
                parameter["password"] = password
                return parameter
            }
            return paramester
        case .signup(let username, let fullName, let password):
            var paramester: [String: Any]?{
                var paramester: [String: Any] = [:]
                paramester["username"] = username
                paramester["fullname"] = fullName
                paramester["password"] = password
                return paramester
            }
            return paramester
        case .warning(let id, let temperature, let isWarning):
            var paramester: [String: Any]?{
                var paramester: [String: Any] = [:]
                paramester["ownerId"] = id
                paramester["temperature"] = temperature
                paramester["isWarning"] = isWarning
                return paramester
            }
            return paramester
        
        default:
            return [:]
        }
    }
    var task: Moya.Task {
        return .requestParameters(parameters: self.parameters! , encoding: parameterEncoding)
    }
    
    var headers: [String : String]? {
        return [:]
    }
    
    
    
}

