//
//  SignupViewRouter.swift
//  Thermometer Smart
//
//  Created by MinhNT-Mac on 6/3/19.
//  Copyright © 2019 MinhNT. All rights reserved.
//

import Foundation
import UIKit
protocol SignupViewRouter {
    func presentHome()
    func presentSignin()
}

class SignupRouterImplemetation: SignupViewRouter
{
    fileprivate weak var signupController: SignupViewController?
    
    init(signupController: SignupViewController) {
        self.signupController = signupController
    }
    func presentHome() {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "searchingBluetooth") as! SearchingBluetoothViewController
        signupController?.present(vc, animated: true, completion: nil)
    }
    func presentSignin() {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "signin") as! LoginViewController
        signupController?.present(vc, animated: true, completion: nil)
    }
}
