//
//  TemperatureRealtimeEntity.swift
//  Thermometer Smart
//
//  Created by MinhNT-Mac on 6/7/19.
//  Copyright © 2019 MinhNT. All rights reserved.
//

import Foundation

import ObjectMapper

class TemperatureRealtimeEntity: NSObject , Mappable {
    
    public var ownerId:String?
    public var temprature:String?
    public var isModeAnalytics:Bool?
    public var isWarning:Bool?
    
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        ownerId <- map["ownerId"]
        temprature <- map["temprature"]
        isModeAnalytics <- map["isModeAnalytics"]
        isWarning <- map["isWarning"]
    }
}
