//
//  HistoryConfig.swift
//  Thermometer Smart
//
//  Created by MinhNT-Mac on 6/6/19.
//  Copyright © 2019 MinhNT. All rights reserved.
//

import Foundation
protocol HistoryConfiguration {
    func configure(historyControler: HistoryViewController)
}
class HistoryConfigurationImplementation:  HistoryConfiguration {
    func configure(historyControler: HistoryViewController) {
        
        let homeGateway = ApiHomeGatewayImplementation()
        let presenter = HistoryPresenterImplementation(view: historyControler , homeGateway: homeGateway)
        historyControler.presenter = presenter
    }
    
    
}
