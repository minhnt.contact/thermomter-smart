//
//  SettingsEntity.swift
//  Thermometer Smart
//
//  Created by MinhNT on 6/5/19.
//  Copyright © 2019 MinhNT. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper
class SettingsEntity: Object, Mappable {
    
    @objc public dynamic var temperatureBot: Double = 33.5
    @objc public dynamic var temperatureTop: Double = 37.5
    @objc public dynamic var phoneSOS: Int = 0966578304
    @objc public dynamic var timeRequest: Int = 1
    @objc public dynamic var timeRepeat: Int = 1
    @objc public dynamic var timeWarning: Int = 1
    @objc public dynamic var isModeAnalysis: Bool = false
    @objc public dynamic var ringTone: String = "warning"
    
    override static func primaryKey() -> String? {
        return "phoneSOS"
    }
    
    required convenience init?(map: Map) {
        self.init()
    }
    func mapping(map: Map) {
        temperatureBot <- map["temperatureBot"]
        temperatureTop <- map["temperatureTop"]
        phoneSOS <- map["phoneSOS"]
        timeRequest <- map["timeRequest"]
        timeWarning <- map["timeWarning"]
        ringTone <- map["ringTone"]
    }
}
extension SettingsEntity
{
    class func saveSettings(_ settingsEntity: SettingsEntity)
    {
        do{
            let realm = try! Realm()
            try realm.write {
                realm.add(settingsEntity,update:true)
            }
        }catch let error as NSError {
            Log.debug(message: error.description)
        }
    }
    
    class func getSettings() -> SettingsEntity?
    {
        do{
            let realm = try! Realm()
            return realm.objects(SettingsEntity.self).first
        }catch let error as NSError {
            Log.debug(message: error.description)
            return nil
        }
        
    }
    
}
