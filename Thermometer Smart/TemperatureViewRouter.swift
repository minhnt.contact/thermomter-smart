//
//  TemperatureViewRouter.swift
//  Thermometer Smart
//
//  Created by MinhNT on 5/22/19.
//  Copyright © 2019 MinhNT. All rights reserved.
//

import Foundation
import UIKit
protocol TemperatureViewRouter {
    func presentWarning()
    
}

class TemperatureRouterImplemetation: TemperatureViewRouter
{
    fileprivate weak var temperatureController: TemperatureViewController?
    
    init(temperatureController: TemperatureViewController) {
        self.temperatureController = temperatureController
    }
    func presentWarning() {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        if #available(iOS 10.0, *) {
            let newViewController = storyBoard.instantiateViewController(withIdentifier: "warningTemperature") as! WarningViewController
            newViewController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            temperatureController!.present(newViewController, animated: true, completion: nil)
        } else {
            // Fallback on earlier versions
        }
        
    }
}
