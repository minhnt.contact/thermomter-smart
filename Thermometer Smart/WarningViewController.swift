//
//  WarningViewController.swift
//  Thermometer Smart
//
//  Created by MinhNT-Mac on 5/28/19.
//  Copyright © 2019 MinhNT. All rights reserved.
//

import UIKit
import AVFoundation
import AudioToolbox
import UserNotifications
@available(iOS 10.0, *)
class WarningViewController: UIViewController, AVAudioPlayerDelegate {
    
    @IBOutlet weak var bgProcess: UIImageView!
    @IBOutlet weak var lbTemarate: UILabel!
    @IBOutlet weak var imgArrowProcess: UIView!
    @IBOutlet weak var viewTitle: UIView!
    @IBOutlet weak var viewContent: UIView!
    @IBOutlet weak var btnCallPhone: UIButton!
    
    var isStop = false
    let notificationCenter = UNUserNotificationCenter.current()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    func setup(){
        NotificationCenter.default.addObserver(self, selector: #selector(showWarning), name: notificationName.showWarning.notification, object: nil)
        viewTitle.setViewRadius()
        viewContent.setViewRadius()
        btnCallPhone.setViewRadius()
    }
    
    @IBAction func back(_ sender: Any) {
        self.dismiss(animated: true)
        
        guard  Thermometer.audioPlayer != nil else { return }
        if Thermometer.audioPlayer.isPlaying {
            Thermometer.audioPlayer.stop()
        }
        
    }
    
    func showData (value: Double){
        DispatchQueue.main.async {
            self.lbTemarate.text = "\(String(format: "%.1f", value))°C"
            UIView.animate(withDuration: 1.5, animations: ({
                self.imgArrowProcess.transform = CGAffineTransform(rotationAngle: self.getDegrees(temperature: value))
            }))
        }
    }
    
    @objc func showWarning(_ notification: NSNotification) {
        DispatchQueue.main.async {
            if let temperature = notification.userInfo?["temperature"] as? Double {
                self.showData(value: Double(temperature / 100))
//
            }
            self.runSoundWarning()
            
        }
    }
    @IBAction func callPhoneSOS(_ sender: Any) {
        if let url = URL(string: "tel://\(DataSingleton.PhoneSOS)"),
            UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:], completionHandler:nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        } else {
            // add error message here
        }
    }
    
    func runSoundWarning() {
        switch UIApplication.shared.applicationState {
        case .active:
            let soundUrl = Bundle.main.url(forResource: DataSingleton.RingTone, withExtension: "mp3")
            do {
                Thermometer.audioPlayer = try AVAudioPlayer(contentsOf: soundUrl!)
                Thermometer.audioPlayer.delegate = self
                try AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback)
                try AVAudioSession.sharedInstance().setActive(true)
            }catch {
                runSoundWarning()
            }
            Thermometer.audioPlayer.play()
            break
        case .inactive:
            scheduleNotification()
            break
        case .background:
            scheduleNotification()
            break
        default:
            break
        }
        
    }
    func scheduleNotification() {
        
        let content = UNMutableNotificationContent()
        content.title = "Warning"
        content.body = "Temperature in dangerous area"
        content.sound = UNNotificationSound(named: UNNotificationSoundName(rawValue: "\(DataSingleton.RingTone).mp3"))
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1,
                                                        repeats: false)
        let identifier = "WarningTemporature"
        let request = UNNotificationRequest(identifier: identifier,
                                            content: content, trigger: trigger)
        let center = UNUserNotificationCenter.current()
        center.add(request, withCompletionHandler: { (error) in
            if let error = error {
                // Something went wrong
            }
        })
        let snoozeAction = UNNotificationAction(identifier: "Snooze",
                                                title: "Snooze", options: [])
        let deleteAction = UNNotificationAction(identifier: "UYLDeleteAction",
                                                title: "Delete", options: [.destructive])
        
        let category = UNNotificationCategory(identifier: "WarningTemporature",
                                              actions: [snoozeAction,deleteAction],
                                              intentIdentifiers: [], options: [])
        center.setNotificationCategories([category])

        content.categoryIdentifier = "WarningTemporature"
    }
    
    
    private func getDegrees(temperature: Double) -> CGFloat
    {
        if temperature < 30 || temperature > 46
        {
            return 0
        }
        else
        {
            return  CGFloat(Double(temperature - 28) * 22.5 / 180 * Double.pi)
        }
    }
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        if !isStop {
            Thermometer.audioPlayer.play()
        }
    }
    
}
@available(iOS 10.0, *)
class UYLNotificationDelegate: NSObject, UNUserNotificationCenterDelegate {
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        // Play sound and show alert to the user
        completionHandler([.alert,.sound])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        
        // Determine the user action
        switch response.actionIdentifier {
        case UNNotificationDismissActionIdentifier:
            print("Dismiss Action")
        case UNNotificationDefaultActionIdentifier:
            print("Default")
        case "Snooze":
            print("Snooze")
        case "Delete":
            print("Delete")
        default:
            print("Unknown action")
        }
        completionHandler()
    }
}
