//
//  UserDetailEntity.swift
//  Thermometer Smart
//
//  Created by MinhNT-Mac on 6/3/19.
//  Copyright © 2019 MinhNT. All rights reserved.
//

import Foundation
import ObjectMapper

class UserEntity: NSObject , Mappable {
    
    public var code:Int?
    public var message:String?
    public var result:UserDetailEntity?

    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        code <- map["code"]
        message <- map["message"]
        result <- map["result"]
    }
}
