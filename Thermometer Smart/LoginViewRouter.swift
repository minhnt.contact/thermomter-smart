//
//  LoginViewRouter.swift
//  Thermometer Smart
//
//  Created by MinhNT-Mac on 6/2/19.
//  Copyright © 2019 MinhNT. All rights reserved.
//

import Foundation
import UIKit
protocol LoginViewRouter {
    func presentHome()
    func presentSignup()
}

class LoginRouterImplemetation: LoginViewRouter
{
    fileprivate weak var loginController: LoginViewController?
    
    init(loginController: LoginViewController) {
        self.loginController = loginController
    }
    func presentHome() {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "searchingBluetooth") as! SearchingBluetoothViewController
        loginController?.present(vc, animated: true, completion: nil)
    }
    func presentSignup() {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "signup") as! SignupViewController
        loginController?.present(vc, animated: true, completion: nil)
    }
}
