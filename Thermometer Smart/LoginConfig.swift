//
//  LoginConfig.swift
//  Thermometer Smart
//
//  Created by MinhNT-Mac on 6/2/19.
//  Copyright © 2019 MinhNT. All rights reserved.
//

import Foundation
protocol LoginConfiguration {
    func configure(loginControler: LoginViewController)
}
class LoginConfigurationImplementation:  LoginConfiguration {
    func configure(loginControler: LoginViewController) {
        
        let router = LoginRouterImplemetation(loginController: loginControler)
        let loginGateway = ApiLoginGatewayImplementation()
        let presenter = LoginPresenterImplementation(view: loginControler, router: router, loginGateway: loginGateway)
        
        loginControler.presenter = presenter
    }
    
    
}
