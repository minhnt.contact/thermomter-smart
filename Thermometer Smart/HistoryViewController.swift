
//
//  HistoryViewController.swift
//  Thermometer Smart
//
//  Created by MinhNT on 5/29/19.
//  Copyright © 2019 MinhNT. All rights reserved.
//

import UIKit

class HistoryViewController: UIViewController,HistoryView {

    @IBOutlet weak var tbHistory: UITableView!
    
    
    var config: HistoryConfiguration = HistoryConfigurationImplementation()
    var presenter: HistoryPeresenter!
    override func viewDidLoad() {
        super.viewDidLoad()
        config.configure(historyControler: self)
        presenter.viewDidLoad()
        setupTableView()
    
    }
    
    
    func setupTableView(){
        tbHistory.delegate = self
        tbHistory.dataSource = self
        tbHistory.register(UINib(nibName: "HistoryTableViewCell", bundle: nil), forCellReuseIdentifier: "HistoryTableViewCell")
    }
    
    func refreshTableView() {
        tbHistory.reloadData()
    }
}
extension HistoryViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter!.numberOfList
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tbHistory.dequeueReusableCell(withIdentifier: "HistoryTableViewCell") as! HistoryTableViewCell
        cell.setData(warningEntity: presenter.getWarningEntity(row: indexPath.row))
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130
    }
    
    
}
