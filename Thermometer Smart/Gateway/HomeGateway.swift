//
//  HomeGateway.swift
//  Thermometer Smart
//
//  Created by MinhNT on 6/5/19.
//  Copyright © 2019 MinhNT. All rights reserved.
//

import Foundation
typealias HomeCompletionHandler = (_ user: Result<WarningEntity>) -> Void
typealias HistoryCompletionHandler = (_ user: Result<WarningEntity>) -> Void

protocol HomeGateway {
    func warning(id: String, temperature: String,isWarning:Bool, completetionHandler: @escaping HomeCompletionHandler)
    func getListWarning(id:String,completetionHandler: @escaping HistoryCompletionHandler)
    func warningRealTime(id: String, temperature: String,isWarning:Bool, completetionHandler: @escaping HomeCompletionHandler)
}
