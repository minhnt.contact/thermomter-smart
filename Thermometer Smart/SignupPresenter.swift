//
//  SignupPresenter.swift
//  Thermometer Smart
//
//  Created by MinhNT-Mac on 6/3/19.
//  Copyright © 2019 MinhNT. All rights reserved.
//

import Foundation
protocol SignupView: class {
    func handleError(title: String, content: String)
}


protocol SignupPresenter {
    
    func viewDidLoad()
    func presentSignin()
    func handlerSignup(username:String,fullName:String,password:String)
}

class SignupPresenterImplementation: SignupPresenter {
    fileprivate weak var view: SignupView?
    internal let router: SignupViewRouter
    var loginGateway: LoginGateway?
    init(view: SignupView, router: SignupViewRouter,loginGateway: LoginGateway) {
        self.view = view
        self.router = router
        self.loginGateway = loginGateway
    }
    
    func viewDidLoad() {
        
    }
    
    func presentSignin(){
        self.router.presentSignin()
    }
    
    func handlerSignup(username:String,fullName:String,password:String){
        LoadingHUDControl.sharedManager.showLoadingHud()
        loginGateway?.signup(username: username, fullName: fullName, password: password, completetionHandler: { (result) in
            switch result {
            case let .success(data):
                if data.code == 200 {
                    UserDetailEntity.saveUser(data.result!)
                    self.router.presentHome()
                }else {
                    self.view?.handleError(title: "Error", content: data.message!)
                }
                LoadingHUDControl.sharedManager.hideLoadingHud()
                break
            case let .failure(error):
                 self.view?.handleError(title: NSLocalizedString("announce", comment: ""), content: error.localizedDescription)
                 LoadingHUDControl.sharedManager.hideLoadingHud()
                break
            }
        })
    }
    
}
