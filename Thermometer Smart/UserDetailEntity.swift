//
//  UserEntity.swift
//  Thermometer Smart
//
//  Created by MinhNT on 5/29/19.
//  Copyright © 2019 MinhNT. All rights reserved.
//

import Foundation
import RealmSwift
import ObjectMapper
class UserDetailEntity: Object, Mappable {
    
    @objc public dynamic var _id: String = ""
    @objc public dynamic var fullname: String = ""
    @objc public dynamic var avatar: String = ""
    @objc public dynamic var username: String = ""
    @objc public dynamic var status: Int = 0
    @objc public dynamic var password: String = ""
    @objc public dynamic var salt: String = ""
    @objc public dynamic var createAt: String = ""
    @objc public dynamic var updateAt: String = ""
    
    override static func primaryKey() -> String? {
        return "_id"
    }
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    
    func mapping(map: Map) {
        _id <- map["id"]
        fullname <- map["fullname"]
        avatar <- map["avatar"]
        username <- map["username"]
        status <- map["status"]
        password <- map["password"]
        salt <- map["salt"]
        createAt <- map["createAt"]
        updateAt <- map["updateAt"]
    }
}
extension UserDetailEntity
{
    class func saveUser(_ userDetailEntity: UserDetailEntity)
    {
        do{

            let realm = try! Realm()
            try realm.write {
                realm.deleteAll()
                realm.add(userDetailEntity,update:false)
            }
        }catch let error as NSError {
            Log.debug(message: error.description)
        }
    }
    
    class func getUser() -> UserDetailEntity?
    {
        do{
            let realm = try! Realm()
            return realm.objects(UserDetailEntity.self).first
        }catch let error as NSError {
            Log.debug(message: error.description)
            return nil
        }
        
    }
    class func deleteUser()
    {
        do{
            let realm = try! Realm()
            try realm.write {
                realm.deleteAll()
            }
        }catch let error as NSError {
            Log.debug(message: error.description)
            
        }
        
    }
    
}
