//
//  ApiTemperatureGateway.swift
//  Thermometer Smart
//
//  Created by MinhNT on 6/6/19.
//  Copyright © 2019 MinhNT. All rights reserved.
//

import Foundation
protocol ApiHomeGateway: HomeGateway {
    
}

class ApiHomeGatewayImplementation: ApiHomeGateway
{
    func warningRealTime(id: String, temperature: String, isWarning: Bool, completetionHandler: @escaping HomeCompletionHandler) {
        
    }
    
    func warning(id: String, temperature: String, isWarning: Bool, completetionHandler: @escaping HomeCompletionHandler) {
        apiProvider.request(TSAPI.warning(id, temperature, isWarning))
            .asObservable().mapObject(WarningEntity.self).subscribe(onNext:{(result) in
                completetionHandler(.success(result))
            }, onError:{(error) in
                completetionHandler(.failure(error))
            })
    }
   
   
    func getListWarning(id: String, completetionHandler: @escaping HistoryCompletionHandler) {
        apiProvider.request(TSAPI.getHistory(id))
            .asObservable().mapObject(WarningEntity.self).subscribe(onNext:{(result) in
                completetionHandler(.success(result))
            }, onError:{(error) in
                completetionHandler(.failure(error))
            })
    }
    
}
