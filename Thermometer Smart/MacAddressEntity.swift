//
//  MacAddressEntity.swift
//  BabyCare
//
//  Created by HOANDHTB on 12/2/18.
//  Copyright © 2018 nava. All rights reserved.
//

import UIKit
import RealmSwift
import ObjectMapper
class MacAddressEntity: Object, Mappable {
    
    @objc public dynamic var mac: String = ""
    @objc public dynamic var uuid: String = ""
    @objc public dynamic var name: String = ""
    override static func primaryKey() -> String? {
        return "uuid"
    }
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        mac <- map["mac"]
        uuid <- map["uuid"]
        name <- map["name"]
    }
}
extension MacAddressEntity
{
    class func savePeripheralDetail(_ macAddressEntity: MacAddressEntity)
    {
        do{
            let realm = RealmConnectorManager.connectDefault()
            try realm?.write {
                realm?.add(macAddressEntity, update: true)
            }
        }catch let error as NSError {
            Log.debug(message: error.description)
        }
    }
    class func getMacEntity() -> MacAddressEntity?
    {
        do{
            let realm = RealmConnectorManager.connectDefault()
            return realm?.objects(MacAddressEntity.self).first
        }catch let error as NSError {
            Log.debug(message: error.description)
            return nil
        }
    }
}
