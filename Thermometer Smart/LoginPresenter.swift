//
//  LoginPresenter.swift
//  Thermometer Smart
//
//  Created by MinhNT-Mac on 6/2/19.
//  Copyright © 2019 MinhNT. All rights reserved.
//

import Foundation
protocol LoginView: class {
    func handleError(title: String, content: String)
}


protocol LoginPresenter {
    
    func viewDidLoad()
    func presentSignup()
    func handlerLogin(username:String,password:String)
}

class LoginPresenterImplementation: LoginPresenter {
    fileprivate weak var view: LoginView?
    internal let router: LoginViewRouter
    var loginGateway: LoginGateway?
    init(view: LoginView, router: LoginViewRouter,loginGateway: LoginGateway) {
        self.view = view
        self.router = router
        self.loginGateway = loginGateway
    }
    
    func viewDidLoad() {
        autoLogin()
    }
    
    func presentSignup(){
        self.router.presentSignup()
    }
    
    func handlerLogin(username:String,password:String){
        LoadingHUDControl.sharedManager.showLoadingHud()
        loginGateway?.login(username: username, password: password, completetionHandler: { (result) in
            switch result {
            case let .success(data):
                if data.code == 200 {
                    data.result?.username = username
                    data.result?.password = password
                    UserDetailEntity.saveUser(data.result!)
                    self.router.presentHome()
                }else {
                    self.view?.handleError(title: "Error", content: data.message!)
                }
                LoadingHUDControl.sharedManager.hideLoadingHud()
                break
            case let .failure(error):
                LoadingHUDControl.sharedManager.showLoadingHud()
                self.view?.handleError(title: NSLocalizedString("announce", comment: ""), content: error.localizedDescription)
                break
            }
        })
    }
    func autoLogin(){
        guard let user = UserDetailEntity.getUser() else { return }
        handlerLogin(username: user.username, password: user.password)
    }
    
}
