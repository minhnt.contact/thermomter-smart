//
//  TemperatureViewController.swift
//  Thermometer Smart
//
//  Created by MinhNT on 5/22/19.
//  Copyright © 2019 MinhNT. All rights reserved.
//

import UIKit
import Charts
import Kingfisher
class TemperatureViewController: UIViewController, TemperatureView {
    
    @IBOutlet weak var lbTemperature: UILabel!
    @IBOutlet weak var imgArrowProcess: UIView!
    @IBOutlet weak var lbDate: UILabel!
    @IBOutlet weak var lbTime: UILabel!
    @IBOutlet var chartView: LineChartView!
    @IBOutlet weak var lbStatus: UILabel!
    @IBOutlet weak var lbTemperatureTop: UILabel!
    private var listTemperature: [TemperatureEntity] = []
    private var temperature: Int = 0
    @IBOutlet weak var viewDetail: UIView!
    @IBOutlet weak var viewBoundetail: UIView!
    var presenter: TemperaturePeresenter?
    var config: TemperatureConfiguration = TemperatureConfigurationImplementation()
    override func viewDidLoad() {
        super.viewDidLoad()
        config.configure(temperatureControler: self)
        presenter?.viewDidLoad()
        setData()
        setupChart()
        NotificationCenter.default.addObserver(self, selector: #selector(noticationDataDevice), name: notificationName.dataThermomete.notification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(showNotConnect), name: notificationName.showNotConnected.notification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(presentWarning), name: notificationName.presentWarning.notification, object: nil)
        viewBoundetail.setViewRadius()
        
    }
    func setupChart() {
        self.chartView.delegate = self
        self.chartView.dragEnabled = true
        self.chartView.setScaleEnabled(false)
        self.chartView.pinchZoomEnabled = true
        self.chartView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0)
        self.chartView.xAxis.drawGridLinesEnabled = false
        self.chartView.xAxis.labelPosition = XAxis.LabelPosition.bottom
        let leftAxis =  self.chartView.leftAxis
        leftAxis.axisMaximum = 42
        leftAxis.axisMinimum = 31
        leftAxis.gridLineDashLengths = [2, 2]
        leftAxis.drawLimitLinesBehindDataEnabled = true
        leftAxis.axisLineColor = UIColor.clear
        self.chartView.rightAxis.enabled = false
        self.chartView.animate(xAxisDuration: 2.5)
        self.chartView.legend.enabled = false
    }
    
    func checkValue(value: Double) {
        
        if value >= DataSingleton.TemperaureTop ||  value <= DataSingleton.TemperatureBot {
            lbStatus.text = "Warning"
            lbStatus.textColor = .red
        }else {
            lbStatus.text = "Safety"
            lbStatus.textColor = .green
        }
    }
    
    func showData (value: Double){
        DispatchQueue.main.async {
            print(value)
            self.lbTemperature.text = "\(String(format: "%.1f", value))°C"
            self.lbTemperatureTop.text = "\(String(format: "%.1f", value))°C"
            self.lbDate.text = Date().convertDateToStringWithDateFormat(dateFormat: "dd/MM/yyyy")
            self.lbTime.text = Date().convertDateToStringWithDateFormat(dateFormat: "HH:mm")
            self.checkValue(value: value)
            UIView.animate(withDuration: 1.5, animations: ({
                self.imgArrowProcess.transform = CGAffineTransform(rotationAngle: self.getDegrees(temperature: value))
            }))
        }
    }
    func setData(){
        DispatchQueue.main.async {
            self.setDataCount(60 * 24,range: 38)
        }
    }

    
    @objc func noticationDataDevice(_ notification: NSNotification)
    {
        if let temperature = notification.userInfo?["data"] as? TemperatureEntity {
            self.showData(value: temperature.value)
            DispatchQueue.main.async {
                self.changeDataChart(temperature: temperature)
            }
        }
    }
    
    @objc func showNotConnect(){
        let alert = UIAlertController(title: "Messsage", message: "Do not connect to device", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func changeDataChart(temperature: TemperatureEntity)
    {
        DispatchQueue.main.async {
            if  self.chartView.data != nil
            {
                let data = self.chartView.data?.dataSets[0] as! LineChartDataSet;
                let values = data.entries
                let calendar = Calendar.current
                let hour = calendar.component(.hour, from: temperature.date)
                let minute = calendar.component(.minute, from: temperature.date)
                if values.count > hour * 60 + minute
                {
                    values[hour * 60 + minute].y = temperature.value
                }
                self.chartView.notifyDataSetChanged()
                self.chartView.data?.notifyDataChanged()
            }
        }
        
    }

    
    func setDataCount(_ count: Int, range: UInt32) {
        let strDateStart = Date().convertDateToStringWithDateFormat(dateFormat: "yyyy-MM-dd 00:00:00")
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date = dateFormatter.date(from: strDateStart!)
        let values = (0..<count).map { (i) -> ChartDataEntry in
            let dateAdd = Calendar.current.date(byAdding: .second, value: 60 * i, to: date!)!;
            return ChartDataEntry(x: Double(i), y: Double(0), data: dateAdd)
        }
        let set = LineChartDataSet(entries: values, label: nil)
        let n = values.count / (4 * 60)
        set.drawValuesEnabled = false
        set.drawCirclesEnabled = false
        set.mode = .linear
        set.setColor(#colorLiteral(red: 0.5529411765, green: 0.5921568627, blue: 0.6117647059, alpha: 1))
        set.lineWidth = 0.5
        set.valueFont = .systemFont(ofSize: 9)
        set.formLineWidth = 1
        set.formSize = 15
        set.fillAlpha = 1
        
        set.fill = Fill(color: #colorLiteral(red: 0.5137254902, green: 0.8549019608, blue: 0.8941176471, alpha: 1))
        set.drawFilledEnabled = true
        
        let data = LineChartData(dataSet: set)
        var indexScroll = Calendar.current.dateComponents([.minute], from: date!, to: Date()).minute!
        chartView.data = data
        chartView.setScaleMinima(CGFloat(n), scaleY: 1)
        let triggerTime = (Int64(NSEC_PER_SEC) * 1)
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(triggerTime) / Double(NSEC_PER_SEC), execute: { () -> Void in
            if indexScroll > 60
            {
                indexScroll -= 60
            }
            self.chartView.moveViewToX(Double(indexScroll))
        })
        chartView.data!.highlightEnabled = false
        LoadingHUDControl.sharedManager.hideLoadingHud()
       

    }
    @objc func presentWarning() {
        self.presenter?.presentWarning()
    }
    
    private func getDegrees(temperature: Double) -> CGFloat
    {
        if temperature < 30 || temperature > 46
        {
            return 0        }
        else
        {
            return  CGFloat(Double(temperature - 28) * 22.5 / 180 * Double.pi)
        }
    }
}

extension TemperatureViewController: ChartViewDelegate
{
    func setup(barLineChartView chartView: BarLineChartViewBase) {
        chartView.chartDescription?.enabled = false
        
        chartView.dragEnabled = true
        chartView.setScaleEnabled(true)
        chartView.pinchZoomEnabled = false
        
        // ChartYAxis *leftAxis = chartView.leftAxis;
        
        let xAxis = chartView.xAxis
        xAxis.labelPosition = .bottom
        
        chartView.rightAxis.enabled = false
    }
    // TODO: Cannot override from extensions
    //extension DemoBaseViewController: ChartViewDelegate {
    func chartValueSelected(_ chartView: ChartViewBase, entry: ChartDataEntry, highlight: Highlight) {
        NSLog("chartValueSelected");
    }
    
    func chartValueNothingSelected(_ chartView: ChartViewBase) {
        NSLog("chartValueNothingSelected");
    }
    
    func chartScaled(_ chartView: ChartViewBase, scaleX: CGFloat, scaleY: CGFloat) {
        
    }
    
    func chartTranslated(_ chartView: ChartViewBase, dX: CGFloat, dY: CGFloat) {
        
    }
}
