//
//  LoginViewController.swift
//  Thermometer Smart
//
//  Created by MinhNT on 5/29/19.
//  Copyright © 2019 MinhNT. All rights reserved.
//

import UIKit

class LoginViewController: BaseViewController,LoginView {

    @IBOutlet weak var tfPassword: UITextField!
    @IBOutlet weak var tfUserName: UITextField!
    @IBOutlet weak var btnLogin: UIButton!
    var presenter: LoginPresenter?
    var config: LoginConfiguration = LoginConfigurationImplementation()
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        config.configure(loginControler: self)
        presenter?.viewDidLoad()
        
    }
    func setupView() {
        btnLogin.setViewRadius()
    }
    
    @IBAction func signup(_ sender: Any) {
        self.presenter?.presentSignup()
    }
    
    @IBAction func login(_ sender: Any) {
        if (tfUserName.text == "" || tfPassword.text == "") {
            return
        }
        presenter?.handlerLogin(username: tfUserName.text!, password: tfPassword.text!)
    }
    
    func handleError(title: String, content: String) {
        self.showAlertWithOnlyCancelAction(title: title, message: content, alertType: .alert, cancelTitle: "Ok", cancelActionHandler: nil)
    }
    
    
    
    
    
}
