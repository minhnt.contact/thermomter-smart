//
//  Define.swift
//  BaseSwift
//
//  Created by nava on 7/13/18.
//  Copyright © 2018 nava. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
struct SegueIdentifier {
    //Explore
    static let goPlayLitsController = "goPlayLitsController"
}

struct ControllerIdentifier {
    //Login
    static let navigationLogin = "navigationLogin"
    
}
struct DateFormat {
    static let yyyyssDash = "yyyy-MM-dd'T'HH:mm:ss"
    static let ddmmSlash = "dd/MM/yyyy HH:mm"
    static let ddMMyyyy = "dd/MM/yyyy"
    static let MMyyyy = "MM/yyyy"
    static let yyyyMMdd = "yyyy/MM/dd"
    static let yyyymsDash = "yyyy-MM-dd'T'HH:mm:ss.SSS"
    static let yyyymsZDash = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    static let HHmm = "HH:mm"
    static let slashddmmyy = "HH:mm dd/MM/yyyy"
    static let yyyymdHmsZ = "yyyy-MM-dd HH:mm:ss ZZZ"
    static let yyyyMdHms = "yyyy/MM/dd HH:mm:ss"
    static let ddMMMMyyyy = "dd MMMM','yyyy"
    static let yyyyMMddHHmmss = "yyyyMMddHHmmss"
}

enum notificationName: String {
    //Login
    
    case skipLogin = "skipLogin"
    case dataThermomete = "dataThermomete"
    case presentTemperature = "presentTemperature"
    case showNotConnected = "showNotConnected"
    case disconnected = "disconnected"
    case showWarning = "showWarning"
    case pushWarning = "pushWarning"
    case presentWarning = "presentWarning"
    case temperatureRealTime = "temperatureRealTime"
    
    var notification: NSNotification.Name{
        return Notification.Name(rawValue: self.rawValue)
    }
}
struct MenuView  {
    public static var viewHome = "viewHome"
    public static var viewSettings = "viewSettings"
    public static var viewHistory = "viewHistory"
}
struct PickerView  {
    public static var RingTone = "RingTone"
    public static var TimeRepeat = "TimeRepeat"
}

struct Thermometer {
    public static var heartMonitor:ThermometerTemperatureMonitor? = nil
    public static var audioPlayer: AVAudioPlayer!
}

struct CodeResponse  {
    public static var success: Int = 200
    public static var failure: Int = 400
}

public class DataSingleton {
    static var displayPeripheral:DisplayPeripheral?
    static var RingTone:String = "warning"
    static var TimeWarning: Int = 1
    static var user: UserEntity?
    static var isWarning: Bool = true
    static var modeAnalysis:Bool = false
    static var TemperatureBot:Double = 35.5
    static var TemperaureTop:Double = 37.5
    static var PhoneSOS:Int = 115
    static var numberWarningOfDay:Int = 0
    static var numberWarningOfMonth:Int = 0
}
