//
//  SocketIO.swift
//  Thermometer Smart
//
//  Created by MinhNT-Mac on 6/7/19.
//  Copyright © 2019 MinhNT. All rights reserved.
//

import Foundation
import SocketIO
class SocketIOManager: NSObject {
    static let sharedInstance = SocketIOManager()
    var currentUser:UserDetailEntity?
    var manager: SocketManager = SocketManager(socketURL: URL(string: "http://devhaichuc.herokuapp.com/")!, config: [.log(true), .compress])
    lazy var socket = manager.defaultSocket
    override init() {
        super.init()
        
    }
    
    @objc func pushRealTime(_ notification: NSNotification) {
        if let data = notification.userInfo?["data"] as? TemperatureRealtimeEntity {
            socket.emit("PushTempratureToServer", with: [data])
        }
        
    }
    func pushTemperature(data: TemperatureRealtimeEntity) {
        if socket.status == .connected {
            socket.emit("PushTempratureToServer", with: [data.ownerId, data.temprature, data.isModeAnalytics,data.isWarning])
        }
    }
    func establishConnection() {
        socket.on(clientEvent: .connect) {data, ack in
            print("socket connected")
        }
        socket.connect()
    }
    
}
