//
//  TemperaturePresenter.swift
//  Thermometer Smart
//
//  Created by MinhNT on 5/22/19.
//  Copyright © 2019 MinhNT. All rights reserved.
//

import Foundation
protocol TemperatureView: class {
    
}


protocol TemperaturePeresenter {
    
    func viewDidLoad()
    func presentWarning()
}

class TemperaturePresenterImplementation: TemperaturePeresenter{
    fileprivate weak var view: TemperatureView?
    internal let router: TemperatureViewRouter
    var homeGateway: HomeGateway?
    
    
    init(view: TemperatureView, router: TemperatureViewRouter,homeGateway: HomeGateway) {
        self.view = view
        self.router = router
        self.homeGateway = homeGateway
    }
    
    func viewDidLoad() {
        NotificationCenter.default.addObserver(self, selector: #selector(pushWarning), name: notificationName.pushWarning.notification, object: nil)
    }
    func presentWarning() {
        self.router.presentWarning()
    }
    
    @objc func pushWarning(_ notification: NSNotification)
    {
        guard let user = UserDetailEntity.getUser() else { return }
        if let temperature = notification.userInfo?["temperature"] as? Double {
             warning(id: user._id, temperature: "\(Double(temperature))", isWarning: true, isModeAnalytics: false)
        }
    }
    func warning(id:String, temperature:String ,isWarning: Bool, isModeAnalytics: Bool){
        homeGateway?.warning(id: id, temperature: temperature, isWarning: isWarning, completetionHandler: { (result) in
            switch result {
            case let .success(data):
                if data.code == 200 {
                    print("request warning suscess")
                }else {
                }
                break
            case let .failure(error):
                break
            }
        })
    }
    
}
